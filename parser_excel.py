from openpyxl import load_workbook


def search_coordinate_by(name, sheet):
    """
    Функция ищет среди всех заголовков в строке 1 и выдает координату нужного заголовка
    :param name: имя нужного заголовка
    :param sheet: переменная таблицы
    :return: координата
    """
    for row in sheet.iter_rows(min_row=1, min_col=3, max_col=35, max_row=1):
        for cell in row:
            if cell.value == name:
                return cell.column


def get_datalist_by_(name, link, title):
    """
    Функция возвращает данные из excel файла по имени столбца
    :param link: адрес файла
    :param title: заголовок листа
    :param name: имя выборки
    :return: список данных (float)
    """
    workbook = load_workbook(link)
    sheet = workbook[title]
    coordinate = search_coordinate_by(name, sheet)
    data = []
    for row in sheet.iter_rows(min_row=2, min_col=coordinate, max_col=coordinate, max_row=771):
        for cell in row:
            data.append(cell.value)
    return data


def get_description_by(link, title):
    """
    Функция возвращает описание областей из excel файла
    :param link: адрес файла
    :param title: заголовок листа
    :return: список областей (string)
    """
    workbook = load_workbook(link)
    sheet = workbook[title]
    data = []
    for row in sheet.iter_rows(min_row=2, min_col=2, max_col=2, max_row=771):
        for cell in row:
            data.append(cell.value)
    return data
