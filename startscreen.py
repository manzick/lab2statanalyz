import sys
from PyQt5.QtWidgets import (
    QWidget,
    QLabel,
    QPushButton,
    QApplication,
    QComboBox
)


class StartScreen(QWidget):
    def __init__(self):
        super().__init__()
        self.label_array = [QLabel('Считать управляющие факторы коррелированными при r = ', self),
                            QLabel('Считать управляемый и управляющие факторы коррелированными при r = ', self),
                            QLabel('Уровень значимости для проверки гипотез = ', self),
                            QLabel('Данные для анализа будут взяты из файла data(2005-2014).xlsx', self)]
        self.combobox_array = [QComboBox(self), QComboBox(self), QComboBox(self)]
        self.start_button = QPushButton('Продолжить', self)
        self.__set_main_info__()
        self.__set_labels__()
        self.__set_comboboxes__()
        self.__set_button__()

    def __start_button_clicked__(self):
        """
        Действие по кнопке продолжить
        """
        data = (float(i.currentText()) for i in self.combobox_array)
        for i in data:
            print(i)

    def __set_labels__(self):
        """
        Настройка текстовых полей
        """
        x = 100
        for label in self.label_array:
            label.move(100, x)
            x += 50
            label.adjustSize()

    def __set_comboboxes__(self):
        """
        Настройка комбобоксов, неожиданно правда?
        """
        self.combobox_array[0].addItems([str(round(p*0.05, 2)) for p in range(0, 20)])
        self.combobox_array[1].addItems([str(round(p*0.05, 2)) for p in range(0, 20)])
        self.combobox_array[2].addItems([str(0.01), str(0.05), str(0.1)])
        for combobox, label in zip(self.combobox_array, self.label_array):
            x = label.width() + label.x()
            y = label.y() - 5
            combobox.move(x, y)
        pass

    def __set_button__(self):
        """
        Свойства кнопки
        """
        self.start_button.setToolTip('Нажмите, чтобы продолжить')
        self.start_button.resize(self.start_button.sizeHint())
        x = self.width() / 2 - self.start_button.width() / 2
        y = self.height() * (6/8)
        self.start_button.move(x, y)
        self.start_button.clicked.connect(self.__start_button_clicked__)

    def __set_main_info__(self):
        """
        Задание информации об окне
        """
        self.setGeometry(300, 600, 300, 200)
        self.setFixedSize(700, 400)
        self.setWindowTitle('Входные данные')
        self.show()


def create():
    app = QApplication(sys.argv)
    ex = StartScreen()
    sys.exit(app.exec_())
